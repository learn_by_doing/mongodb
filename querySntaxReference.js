db.movieDetails.find({ runtime: { $gt: 90 } })

db.movieDetails.find({ runtime: { $gt: 90 } }, { _id: 0, title: 1, runtime: 1 })

db.movieDetails.find({ runtime: { $gt: 90, $lt: 120 } }, { _id: 0, title: 1, runtime: 1 })

db.movieDetails.find({ runtime: { $gte: 90, $lte: 120 } }, { _id: 0, title: 1, runtime: 1 })

db.movieDetails.find({ runtime: { $gte: 180 }, "tomato.meter": 100 }, { _id: 0, title: 1, runtime: 1 })

db.movieDetails.find({ rated: { $ne: "UNRATED" } }, { _id: 0, title: 1, rated: 1 })

db.movieDetails.find({ rated: { $in: ["G", "PG"] } }, { _id: 0, title: 1, rated: 1 })

db.movieDetails.find({ rated: { $in: ["G", "PG", "PG-13"] } }, { _id: 0, title: 1, rated: 1 }).pretty()

db.movieDetails.find({ rated: { $in: ["R", "PG-13"] } }, { _id: 0, title: 1, rated: 1 }).pretty()

db.moviesDetails.find({ mpaaRating: { $exists: true } })

db.moviesDetails.find({ mpaaRating: { $exists: false } })

db.movieDetails.find({ mpaaRating: null })

db.movieDetails.find({})

db.movies.find({ viewerRating: { $type: "int" } }).pretty()

db.movies.find({ viewerRating: { $type: "double" } }).pretty()


db.movieDetails.find({
    $or: [{ "tomato.meter": { $gt: 95 } },
    { "metacritic": { $gt: 88 } }]
},
    { _id: 0, title: 1, "tomato.meter": 1, "metacritic": 1 })

db.movieDetails.find({
    $and: [{ "tomato.meter": { $gt: 95 } },
    { "metacritic": { $gt: 88 } }]
},
    { _id: 0, title: 1, "tomato.meter": 1, "metacritic": 1 })

db.movieDetails.find({
    "tomato.meter": { $gt: 95 },
    "metacritic": { $gt: 88 }
},
    { _id: 0, title: 1, "tomato.meter": 1, "metacritic": 1 })

db.movieDetails.find({
    $and: [{ "metacritic": { $ne: null } },
    { "metacritic": { $exists: true } }]
},
    { _id: 0, title: 1, "metacritic": 1 })

db.movieDetails.find({
    $and: [{ "metacritic": null },
    { "metacritic": { $exists: true } }]
},
    { _id: 0, title: 1, "metacritic": 1 })

db.movieDetails.find({ genres: { $all: ["Comedy", "Crime", "Drama"] } },
    { _id: 0, title: 1, genres: 1 }).pretty()




const courses = await Course
    .find({ author: 'Nitesh Nandan', isPublished: ture })
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .limit(10)
    .sort({ name: 1 })
    .select({ name: 1, tags: 1 });


```
Types of Variable while creating schema (mongoose)
    String
    Number
    Date
    Buffer
    Boolean
    Array
Comparision Query in MongoDb
    eq (equal)
    ne (not equal)
    gt (greater than)
    gte (greater than or equal to)
    lt (less than)
    lte (less than or equal to)
    in
    nin (not in)
```