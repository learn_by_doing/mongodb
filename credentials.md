```

Hostname: cluster0-shard-00-00-jxeqq.mongodb.net
Username: m001-student
Password: m001-mongodb-basics
Replica Set Name: Cluster0-shard-0
Read Preference: Primary Prefered

```


```

mongo "mongodb://cluster0-shard-00-00-jxeqq.mongodb.net:27017,cluster0-shard-00-01-jxeqq.mongodb.net:27017,cluster0-shard-00-02-jxeqq.mongodb.net:27017/test?replicaSet=Cluster0-shard-0" --authenticationDatabase admin --ssl --username m001-student --password m001-mongodb-basics

```

```
mongo "mongodb+srv://sandbox-q0x9p.mongodb.net/test" --username m001-student --password test123

```
